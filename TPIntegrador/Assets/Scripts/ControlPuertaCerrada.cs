using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPuertaCerrada : MonoBehaviour
{

    private bool llaveAmarilla;
    private bool llaveRoja;
    private bool llaveGanar;
    public Light luzPuerta;

    public GameObject hudLlaveAmarilla;
    public GameObject hudLlaveRoja;
    public GameObject hudLlaveVerde;



    void Update()
    {
        llaveAmarilla = ControlJugador.llaveAmarilla;
        llaveRoja = ControlJugador.llaveRoja;
        llaveGanar = ControlJugador.llaveGanar;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(llaveAmarilla == true && collision.gameObject.CompareTag("Player") == true && gameObject.CompareTag("puertaAmarilla") == true)
        {
            Destroy(gameObject);            
            GestorDeAudio.instancia.ReproducirSonido("abrirPuerta");
            hudLlaveAmarilla.SetActive(false);
        }

        if (llaveRoja == true && collision.gameObject.CompareTag("Player") ==true && gameObject.CompareTag("puertaRoja") == true)
        {            
            Destroy(gameObject);
            luzPuerta.enabled = false;
            GestorDeAudio.instancia.ReproducirSonido("abrirPuerta");
            hudLlaveRoja.SetActive(false);
        }
        if (llaveGanar == true && collision.gameObject.CompareTag("Player") == true && gameObject.CompareTag("puertaGanar") == true)
        {            
            Destroy(gameObject);            
            GestorDeAudio.instancia.ReproducirSonido("abrirPuerta");
            hudLlaveVerde.SetActive(false);
        }

    }
}
