using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDisparoJefeFinal : MonoBehaviour
{
    bool volverAPulsar = true;
    public GameObject flecha;
    public Camera rayCastArco1;
    public Camera rayCastArco2;
    public Camera rayCastArco3;
    public Camera rayCastArco4;
    public Camera rayCastArco5;

    void Start()
    {
        rayCastArco1.enabled = false;
        rayCastArco2.enabled = false;
        rayCastArco3.enabled = false;
        rayCastArco4.enabled = false;
        rayCastArco5.enabled = false;
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 2)
    {

        float tiempoRestante = valorCronometro;
        while (tiempoRestante >= 2)
        {
            yield return new WaitForSeconds(2.0f);

            volverAPulsar = true;
            tiempoRestante--;
        }

    }

    void Update()
    {

        if (volverAPulsar== true)
        {
            volverAPulsar = false;
            GestorDeAudio.instancia.ReproducirSonido("proyectilJefe");
            //Raycast1
            Ray ray = rayCastArco1.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));            
            GameObject pro;
            pro = Instantiate(flecha, ray.origin, transform.rotation);
            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(rayCastArco1.transform.forward * 10, ForceMode.Impulse);
            //Collider col = pro.GetComponent<MeshCollider>();

            Ray ray2 = rayCastArco2.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            GameObject pro2;
            pro2 = Instantiate(flecha, ray2.origin, transform.rotation);
            Rigidbody rb2 = pro2.GetComponent<Rigidbody>();
            rb2.AddForce(rayCastArco2.transform.forward * 10, ForceMode.Impulse);

            Ray ray3 = rayCastArco3.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            GameObject pro3;
            pro3 = Instantiate(flecha, ray3.origin, transform.rotation);
            Rigidbody rb3 = pro3.GetComponent<Rigidbody>();
            rb3.AddForce(rayCastArco3.transform.forward * 10, ForceMode.Impulse);

            Ray ray4 = rayCastArco4.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            GameObject pro4;
            pro4 = Instantiate(flecha, ray4.origin, transform.rotation);
            Rigidbody rb4 = pro4.GetComponent<Rigidbody>();
            rb4.AddForce(rayCastArco4.transform.forward * 10, ForceMode.Impulse);

            Ray ray5 = rayCastArco5.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            GameObject pro5;
            pro5 = Instantiate(flecha, ray5.origin, transform.rotation);
            Rigidbody rb5 = pro5.GetComponent<Rigidbody>();
            rb5.AddForce(rayCastArco5.transform.forward * 10, ForceMode.Impulse);

            Destroy(pro, 5);
            Destroy(pro2, 5);
            Destroy(pro3, 5);
            Destroy(pro4, 5);
            Destroy(pro5, 5);


            StartCoroutine(ComenzarCronometro(2));


        }


    }
}
