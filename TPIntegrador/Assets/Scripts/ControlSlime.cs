using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlSlime : MonoBehaviour
{
    public GameObject jugador;
    public int rapidez;
    private Rigidbody rb;
    private bool saltar = true;
    private float magnitudSalto;

    //SPAWNS DE LOS SLIMES
    public GameObject slimeGrande;
    public GameObject slimeMediano;
    public GameObject slimePequenio;
    public GameObject particula;
    private List<GameObject> listaEnemigos = new List<GameObject>();
    private int sonidoDanio = 1;
    /**********VARIABLES GLOBALES***********/
    private int hp;
    private int danioJugador;
    private bool jugadorMuerto;
    /****************************************/


    public void ParticulaMuerte()
    {
        GameObject particulas = Instantiate(particula, transform.position, Quaternion.identity) as GameObject;
        Destroy(particulas, 2);
    }



    void Start()
    {
        if(gameObject == slimeGrande)
        {
            hp = 60;
            
        }
        if(gameObject == slimeMediano)
        {
            hp = 40;
            
        }
        if(gameObject == slimePequenio)
        {            
            hp = 20;
            
        }
        rb = GetComponent<Rigidbody>();
        jugador = GameObject.Find("Jugador");

    }

    public void recibirDanio()
    {
        hp = hp - danioJugador;

        if (hp <= 0)
        {
            this.desaparecer();
            GestorDeAudio.instancia.ReproducirSonido("muerteSlime");
            ParticulaMuerte();
        }
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 2)
    {

        float tiempoRestante = valorCronometro;
        while (tiempoRestante >= 2)
        {
            yield return new WaitForSeconds(2.0f);

            saltar = true;
            tiempoRestante--;
        }

    }

    private void Update()
    {
        jugadorMuerto = ControlJugador.jugadorMuerto;
        if (jugadorMuerto == true)
        {
            foreach (GameObject item in listaEnemigos)
            {
                Destroy(item);
            }
            Destroy(gameObject);
        }


        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);

        if (saltar == true)
        {
            saltar = false;
                        
            magnitudSalto = 7.0f;
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);            
            StartCoroutine(ComenzarCronometro(2));


        }
    }


    private void desaparecer()
    {
        Destroy(gameObject);

        if(gameObject == slimeGrande)
        {
            listaEnemigos.Add(Instantiate(slimeMediano, transform.position, Quaternion.identity));
            listaEnemigos.Add(Instantiate(slimeMediano, transform.position, Quaternion.identity));
        }
        if(gameObject == slimeMediano)
        {
            listaEnemigos.Add(Instantiate(slimePequenio, transform.position, Quaternion.identity));
            listaEnemigos.Add(Instantiate(slimePequenio, transform.position, Quaternion.identity));
            listaEnemigos.Add(Instantiate(slimePequenio, transform.position, Quaternion.identity));
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("haceDanio"))
        {
            danioJugador = ControlJugador.danioJugador;
            recibirDanio();
            
            if (sonidoDanio == 1)
            {
                GestorDeAudio.instancia.ReproducirSonido("enemigoRecibeDanio1");
                sonidoDanio++;
            }
            else if (sonidoDanio == 2)
            {
                GestorDeAudio.instancia.ReproducirSonido("enemigoRecibeDanio2");
                sonidoDanio--;
            }

            //GestorDeAudio.instancia.ReproducirSonido("enemigoRecibeDanio");
        }

        if(gameObject == slimePequenio && collision.gameObject.CompareTag("Player"))
        {
            this.desaparecer();
            GestorDeAudio.instancia.ReproducirSonido("muerteSlime");
            ParticulaMuerte();
        }

    }

}
