using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.Random;

public class ControlJefeFinal : MonoBehaviour
{
    public GameObject jugador;
    public int rapidez;
    private Rigidbody rb;
    private int sonidoDanio = 1;
    bool mirarJugador = true;
    public GameObject particula;
    /******** INVOCACIONES ENEMIGOS **********/
    bool invocarEnemigos = false;
    bool invocarEnemigosPrimeraVez = true;
    public GameObject slimeGrande;
    public GameObject esqueleto;    
    private List<GameObject> listaEnemigos = new List<GameObject>();
    /**********VARIABLES GLOBALES***********/
    public static int hpJefe;
    private int danioJugador;
    private bool jugadorMuerto;
    /****************************************/


    void Start()
    {
        hpJefe = 500;
        rb = GetComponent<Rigidbody>();
        jugador = GameObject.Find("Jugador");
        transform.LookAt(jugador.transform);
        
    }
    public IEnumerator ComenzarCronometro(float valorCronometro = 2)
    {

        float tiempoRestante = valorCronometro;
        while (tiempoRestante >= 2)
        {
            yield return new WaitForSeconds(2.0f);

            mirarJugador = true;
            tiempoRestante--;
        }

    }

    public void ParticulaMuerte()
    {
        GameObject particulas = Instantiate(particula, transform.position, Quaternion.identity) as GameObject;
        Destroy(particulas, 2);
    }
    public void recibirDanio()
    {
        hpJefe = hpJefe - danioJugador;

        if (hpJefe <= 0)
        {
            this.desaparecer();
            foreach (GameObject item in listaEnemigos)
            {
                Destroy(item);
            }

            GestorDeAudio.instancia.ReproducirSonido("muerteJefe");
            ParticulaMuerte();
        }
    }

    public IEnumerator InvocarEnemigos(float valorCronometro = 15)
    {

        float tiempoRestante2 = valorCronometro;
        while (tiempoRestante2 >= 15)
        {
            yield return new WaitForSeconds(15.0f);

            invocarEnemigos = true;
            tiempoRestante2--;
        }

    }
    public IEnumerator InvocarEnemigosPrimeraVez(float valorCronometro = 3)
    {

        float tiempoRestante3 = valorCronometro;
        while (tiempoRestante3 >= 3)
        {
            yield return new WaitForSeconds(3.0f);

            invocarEnemigos = true;
            tiempoRestante3--;
        }

    }



    private void Update()
    {
        jugadorMuerto = ControlJugador.jugadorMuerto;        
        if (jugadorMuerto == true)
        {                        
            foreach (GameObject item in listaEnemigos)
            {
                Destroy(item);
            }
            this.desaparecer();
        }
        
        if (mirarJugador == true)
        {
            mirarJugador = false;
            transform.LookAt(jugador.transform);
            StartCoroutine(ComenzarCronometro(2));


        }
        if(invocarEnemigosPrimeraVez == true)
        {
            StartCoroutine(InvocarEnemigosPrimeraVez(3));
            invocarEnemigosPrimeraVez = false;
        }
        if (invocarEnemigos==true)
        {
            invocarEnemigos = false;
            GestorDeAudio.instancia.ReproducirSonido("invocacionEnemigos");
            listaEnemigos.Add(Instantiate(esqueleto, new Vector3(-16.2f, 14.35f, 65.65f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(slimeGrande, new Vector3(-3, 14.35f, 68.8f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(esqueleto, new Vector3(11.8f, 14.35f, 65.65f), Quaternion.identity));
            StartCoroutine(InvocarEnemigos(15));
        }        

    }
    private void desaparecer()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("haceDanio"))
        {
            danioJugador = ControlJugador.danioJugador;
            recibirDanio();

            if (sonidoDanio == 1)
            {
                GestorDeAudio.instancia.ReproducirSonido("jefeRecibeDanio1");
                sonidoDanio++;
            }
            else if (sonidoDanio == 2)
            {
                GestorDeAudio.instancia.ReproducirSonido("jefeRecibeDanio2");
                sonidoDanio++;
            }else if(sonidoDanio== 3)
            {
                GestorDeAudio.instancia.ReproducirSonido("jefeRecibeDanio3");
                sonidoDanio = 1;
            }


        }

    }

}
