using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDisparoEsqueleto : MonoBehaviour
{
    bool volverAPulsar = true;
    public GameObject flecha;
    public Camera rayCastArco;


    void Start()
    {
        rayCastArco.enabled = false;
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 2)
    {

        float tiempoRestante = valorCronometro;
        while (tiempoRestante >= 2)
        {
            yield return new WaitForSeconds(2.0f);

            volverAPulsar = true;
            tiempoRestante--;
        }

    }

    void Update()
    {

        if (volverAPulsar== true)
        {
            volverAPulsar = false;
            Ray ray = rayCastArco.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GestorDeAudio.instancia.ReproducirSonido("esqueletoDisparo");
            GameObject pro;
            pro = Instantiate(flecha, ray.origin, transform.rotation);
            Collider col = pro.GetComponent<MeshCollider>();
            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(rayCastArco.transform.forward * 10, ForceMode.Impulse);



            Destroy(pro, 5);


            StartCoroutine(ComenzarCronometro(2));


        }


    }
}
