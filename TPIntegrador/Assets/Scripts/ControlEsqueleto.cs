using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.Random;

public class ControlEsqueleto : MonoBehaviour
{
    public GameObject jugador;
    public int rapidez;
    private Rigidbody rb;
    private int sonidoDanio = 1;
    public GameObject particula;
    /**********VARIABLES GLOBALES***********/
    private int hp;
    private int danioJugador;
    /****************************************/
    

    void Start()
    {
        hp = 40;
        rb = GetComponent<Rigidbody>();
        jugador = GameObject.Find("Jugador");
        
    }
    public void ParticulaMuerte()
    {
        GameObject particulas = Instantiate(particula, transform.position, Quaternion.identity) as GameObject;
        Destroy(particulas, 2);
    }
    public void recibirDanio()
    {
        hp = hp - danioJugador;

        if (hp <= 0)
        {
            this.desaparecer();
            GestorDeAudio.instancia.ReproducirSonido("muerteEsqueleto");
            ParticulaMuerte();
        }
    }

    private void Update()
    {
        
        
        transform.LookAt(jugador.transform);

    }


    private void desaparecer()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("haceDanio"))
        {
            danioJugador = ControlJugador.danioJugador;
            recibirDanio();
            
            if(sonidoDanio ==1)
            {
                GestorDeAudio.instancia.ReproducirSonido("enemigoRecibeDanio1");
                sonidoDanio++;
            }
            else if(sonidoDanio == 2)
            {
                GestorDeAudio.instancia.ReproducirSonido("enemigoRecibeDanio2");
                sonidoDanio--;
            }


        }

    }

}
