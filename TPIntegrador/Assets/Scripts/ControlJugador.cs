using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ControlJugador : MonoBehaviour
{
    private Rigidbody rb;
    public int rapidez;
    public GameObject Espada;
    //TEXTOS//
    public TMPro.TMP_Text textoDanioJugador;
    public TMPro.TMP_Text textoVidaJugador;
    public TMPro.TMP_Text textoGameOver;
    public TMPro.TMP_Text textoVictoria;
    public TMPro.TMP_Text textoControles;
    public TMPro.TMP_Text textoPowerUp;
    public TMPro.TMP_Text textoVidaJefe;
    public GameObject hudLlaveAmarilla;
    public GameObject hudLlaveRoja;
    public GameObject hudLlaveVerde;
    public GameObject hudIconoVida;
    public GameObject hudIconoDanio;

    //******************//
    private int vidaJugador;
    public GameObject particula;
    public Light luzLlaveAmarilla;
    public Light luzLlaveRoja;
    public Light luzFinal;
    public GameObject postProcessing;
    public GameObject puertaRojaJefe;
    //Enemigos//
    private bool triggerUno = true;
    private bool triggerDos = true;
    private bool triggerTres = true;
    private bool triggerCuatro = true;
    private bool triggerCinco = true;
    private bool triggerJefe = true;
    private bool muerteJefe = false;
    public GameObject slimeGrande;
    public GameObject slimeMediano;
    public GameObject esqueleto;
    public GameObject jefeFinal;
    public GameObject llaveObjetoGanar;
    public Camera camaraJugador;
    public Camera camaraVictoria;
    private List<GameObject> listaEnemigos = new List<GameObject>();
    private List<GameObject> listaJefeFinal = new List<GameObject>();

    /**********VARIABLES GLOBALES***********/
    public static bool powerUpProyectil = false;
    public static int danioJugador = 50;
    public static bool jugadorMuerto = false;
    public static bool llaveAmarilla = false;
    public static bool llaveRoja = false;
    public static bool llaveGanar = false;    
    private int hpJefe = 500;
    /****************************************/
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        danioJugador = 10;
        vidaJugador = 100;
        jugadorMuerto = false;
        powerUpProyectil = false;
        llaveAmarilla = false;
        llaveRoja = false;
        llaveGanar = false;
        textoPowerUp.enabled = false;        
        textoDanioJugador.text = ("1"+danioJugador+"%");
        textoVidaJugador.text = (vidaJugador.ToString());
        textoGameOver.text = "";
        textoVidaJefe.text = "";
        textoVictoria.enabled = false;
        GestorDeAudio.instancia.ReproducirSonido("musicaJuego");
        luzFinal.enabled = false;
        camaraVictoria.enabled = false;
        hudLlaveAmarilla.SetActive(false);
        hudLlaveRoja.SetActive(false);
        hudLlaveVerde.SetActive(false);
        puertaRojaJefe.SetActive(false);
    }
    public void ParticulaMuerte()
    {
        GameObject particulas = Instantiate(particula, transform.position, Quaternion.identity) as GameObject;
        Destroy(particulas, 2);
    }
    private void FixedUpdate()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");
        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);
        rb.AddForce(vectorMovimiento * rapidez);



    }

    void Update()
    {
        /********** GOD MODE ****************/
        if (Input.GetKeyDown("t"))
        {
            Debug.Log("GOD MODE ON");
            vidaJugador = 999999;
            danioJugador = 99999;
            textoVidaJugador.text = (vidaJugador.ToString());
            textoDanioJugador.text = (danioJugador + "%");
        }
        /********** GOD MODE ****************/
        if (Input.GetKeyDown("x"))
        {
            EsconderTextos();
        }
        if (triggerJefe == false && hpJefe > 0)
        {
            hpJefe = ControlJefeFinal.hpJefe;
            textoVidaJefe.text = ("ROSCHARCH: " + hpJefe);
        }
        if(hpJefe <= 0 && muerteJefe == false)

        {
            textoVidaJefe.text = "ROSCHARCH: 0";
            textoVidaJefe.text = "";
            muerteJefe = true;            
            listaEnemigos.Add(Instantiate(llaveObjetoGanar, new Vector3(-3.1f, 16, 49.68f), Quaternion.identity));
        }



    }
    void EsconderTextos()
    {
        textoControles.enabled = false;
        textoPowerUp.enabled = false;
    }

    private void OnCollisionEnter(Collision collision)
    {       
       
        if (collision.gameObject.CompareTag("enemigo") == true)
        {
            vidaJugador = vidaJugador - 25;
            textoVidaJugador.text = (vidaJugador.ToString());
            GestorDeAudio.instancia.ReproducirSonido("jugadorRecibeDanio");
            if (vidaJugador <= 0)
            {
                jugadorMuerto = true;
                foreach (GameObject item in listaEnemigos)
                {
                    Destroy(item);
                }                
                gameObject.SetActive(false);
                GestorDeAudio.instancia.ReproducirSonido("muerteJugador");
                textoGameOver.text = "GAME OVER!! PRESIONA R PARA REINICIAR";
                ParticulaMuerte();
                
            }
            
        }

    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("powerUp") == true)
        {
            collider.gameObject.SetActive(false);
            powerUpProyectil = true;
            Destroy(collider.gameObject);
            textoPowerUp.enabled = true;            
            GestorDeAudio.instancia.ReproducirSonido("powerUpEspada");
        }

        if (collider.gameObject.CompareTag("powerUpDmg") == true)
        {
            collider.gameObject.SetActive(false);
            Destroy(collider.gameObject);
            danioJugador += 10;            
            textoDanioJugador.text = ("1" + danioJugador + "%");

            GestorDeAudio.instancia.ReproducirSonido("powerUpEspada");
        }


        if (collider.gameObject.CompareTag("powerUpVida") == true)
        {
            collider.gameObject.SetActive(false);
            Destroy(collider.gameObject);
            vidaJugador += 25;
            textoVidaJugador.text = (vidaJugador.ToString());
            GestorDeAudio.instancia.ReproducirSonido("powerUpVida");
        }

        if (collider.gameObject.CompareTag("llaveAmarilla") == true)
        {
            hudLlaveAmarilla.SetActive(true);
            collider.gameObject.SetActive(false);
            Destroy(collider.gameObject);
            llaveAmarilla = true;
            luzLlaveAmarilla.enabled = false;
            GestorDeAudio.instancia.ReproducirSonido("agarrarLlave");
        }
        if (collider.gameObject.CompareTag("llaveRoja") == true)
        {
            hudLlaveRoja.SetActive(true);
            collider.gameObject.SetActive(false);
            Destroy(collider.gameObject);
            llaveRoja = true;
            luzLlaveRoja.enabled = false;
            GestorDeAudio.instancia.ReproducirSonido("agarrarLlave");
        }
        if (collider.gameObject.CompareTag("llaveGanar") == true)
        {
            foreach (GameObject item in listaEnemigos)
            {
                Destroy(item);
            }
            hudLlaveVerde.SetActive(true);
            collider.gameObject.SetActive(false);
            Destroy(collider.gameObject);
            llaveGanar = true;
            Destroy(postProcessing);
            luzFinal.enabled = true;            
            GestorDeAudio.instancia.ReproducirSonido("agarrarLlave");
        }
        if (collider.gameObject.CompareTag("triggerGanar") == true)
        {
            hudIconoDanio.SetActive(false);
            hudIconoVida.SetActive(false);
            textoDanioJugador.text = "";
            textoVidaJugador.text = "";
            GestorDeAudio.instancia.PausarSonido("musicaJuego");
            gameObject.SetActive(false);
            camaraJugador.enabled = false;
            camaraVictoria.enabled = true;
            textoVictoria.enabled = true;
            GestorDeAudio.instancia.ReproducirSonido("victoria");
        }
        /******** DA�O POR LAVA SE CAMBIA DE ONTRIGGER A ONCOLLISION
        if (collider.gameObject.CompareTag("lava") == true)
        {
            vidaJugador = vidaJugador - 25;
            Debug.Log("Toco lava");
            textoVidaJugador.text = (vidaJugador.ToString());
            GestorDeAudio.instancia.ReproducirSonido("jugadorRecibeDanio");
            if (vidaJugador <= 0)
            {
                gameObject.SetActive(false);
                GestorDeAudio.instancia.ReproducirSonido("muerteJugador");
                textoGameOver.text = "GAME OVER!! PRESIONA R PARA REINICIAR";
                ParticulaMuerte();
            }

        }
        */
        /******* SPAWN ENEMIGOS TRIGGER **************/

        if (collider.gameObject.CompareTag("triggerUno") == true && triggerUno == true)
        {
            listaEnemigos.Add(Instantiate(slimeGrande, new Vector3(-12, 13.7f, -1.91f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(slimeMediano, new Vector3(7, 13.7f, -1.91f), Quaternion.identity));
            triggerUno = false;

        }

        if (collider.gameObject.CompareTag("triggerDos") == true && triggerDos == true)
        {
            listaEnemigos.Add(Instantiate(esqueleto, new Vector3(-22.76f, 14.52f, 33.27f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(esqueleto, new Vector3(-42, 14.52f, 12f), Quaternion.identity));
            triggerDos = false;

        }

        if (collider.gameObject.CompareTag("triggerTres") == true && triggerTres == true)
        {
            listaEnemigos.Add(Instantiate(esqueleto, new Vector3(31.3f, 14, 31.81f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(slimeGrande, new Vector3(32.45f, 14, 23), Quaternion.identity));
            listaEnemigos.Add(Instantiate(esqueleto, new Vector3(31.3f, 14, 14.34f), Quaternion.identity));
            triggerTres = false;

        }
        if (collider.gameObject.CompareTag("triggerCuatro") == true && triggerCuatro == true)
        {
            listaEnemigos.Add(Instantiate(slimeMediano, new Vector3(44.6f, 14, 63), Quaternion.identity));
            listaEnemigos.Add(Instantiate(slimeGrande, new Vector3(53.5f, 14, 55), Quaternion.identity));
            listaEnemigos.Add(Instantiate(esqueleto, new Vector3(50.4f, 14, 63), Quaternion.identity));
            listaEnemigos.Add(Instantiate(esqueleto, new Vector3(57.4f, 14, 63), Quaternion.identity));
            listaEnemigos.Add(Instantiate(slimeMediano, new Vector3(62.1f, 14, 63), Quaternion.identity));
            triggerCuatro = false;

        }

        if (collider.gameObject.CompareTag("triggerCinco") == true && triggerCinco == true)
        {
            listaEnemigos.Add(Instantiate(esqueleto, new Vector3(64.7f, 14, -14.5f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(esqueleto, new Vector3(58.5f, 14, -14.5f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(slimeGrande, new Vector3(54, 14, -14.5f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(esqueleto, new Vector3(50.4f, 14, -14.5f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(esqueleto, new Vector3(44.8f, 14, -14.5f), Quaternion.identity));
            triggerCinco = false;

        }
        /************** JEFE FINAL ************/
        if (collider.gameObject.CompareTag("triggerJefe") == true && triggerJefe == true)
        {
            puertaRojaJefe.SetActive(true);
            listaJefeFinal.Add(Instantiate(jefeFinal, new Vector3(-2.74f, 20.14f, 78.65f), Quaternion.identity));
            GestorDeAudio.instancia.ReproducirSonido("risaJefe");
            textoVidaJefe.text = "ROSCHARCH: 500";
            triggerJefe = false;            
        }
        
    }


}

