using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEspada : MonoBehaviour
{
    bool volverAPulsar = true;
    public GameObject proyectil;
    public Camera rayCastEspada;
    private bool powerUpProyectil;

    void Start()
    {
        rayCastEspada.enabled = false;        

    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 1)
    {

        float tiempoRestante = valorCronometro;
        while (tiempoRestante >= 1)
        {
            yield return new WaitForSeconds(1.0f);

            volverAPulsar = true;
            tiempoRestante--;
        }

    }

    void Update()
    {
        powerUpProyectil = ControlJugador.powerUpProyectil;

        if (Input.GetKeyDown("i") && volverAPulsar && powerUpProyectil == true)
        {
            volverAPulsar = false;
            Ray ray = rayCastEspada.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GestorDeAudio.instancia.ReproducirSonido("espadaProyectil");
            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);
            Collider col = pro.GetComponent<MeshCollider>();
            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(rayCastEspada.transform.forward * 15, ForceMode.Impulse);



            Destroy(pro, 5);


            StartCoroutine(ComenzarCronometro(1));


        }


    }


}


